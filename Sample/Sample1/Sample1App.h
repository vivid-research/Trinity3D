#pragma once

#include "Application.h"
#include "Mesh3D.h"


class Sample1App :
    public App::Application
{
public:

    void InitApp();
    void UpdateApp();
    void RenderApp();
    void ResizeApp(int w,int h);

private:

    Data::Mesh3D* m1;

};

