#pragma once
#include "glm/glm.hpp"
#include <vector>
#include "DataTypes.h"

namespace Backend {

	class MeshBuffer
	{
	public:
		MeshBuffer() {

		}
		MeshBuffer(int vertices, int indices);
		virtual void CreateBuffer(int vertices, int indices) {};

		void SetSize(int vertices, int indices)
		{
			Vertices.resize(vertices);
			Tris.resize(indices / 3);
		}

		void SetVertex(int id, Vertex3D vert) {

			Vertices[id] = vert;

		}

		void SetTri(int id, Tri t) {

			Tris[id] = t;

		}

		int GetNumVertices() {
			return nVertices;
		}

		int GetNumIndices() {
			return nIndices;
		}

		virtual void Finalize() {};

	protected:

		int nVertices, nIndices;
		std::vector<Vertex3D> Vertices;
		std::vector<Tri> Tris;

	};


}