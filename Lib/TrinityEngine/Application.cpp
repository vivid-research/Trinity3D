#include "pch.h"
#include "Application.h"
#include "GBEDirectX12.h"

namespace App {

	Application::Application() {

		//appMetrics = metrics;
		Main = this;


	}

	Application* Application::Main = nullptr;

	void Application::SetMetrics(AppMetrics metrics) {

		appMetrics = metrics;

	}

	void Application::RunApp() {

		switch (appMetrics.Api) {
		case GraphicsApi::DirectX12:

			gbe = new Backend::GBEDirectX12();

			break;

		}

		gbe->SetClearColor(0, 1, 1);

		gbe->CreateWindow(appMetrics.WindowWidth, appMetrics.WindowHeight, appMetrics.FullScreen);

		InitApp();

		while (true) {

			UpdateApp();

			gbe->BeginFrame();

			RenderApp();

			gbe->EndFrame();

			gbe->SwapBuffers();


		}


	}



	Backend::MeshBuffer* Application::CreateMeshBuffer(int vertices, int indices) {

		return gbe->CreateMeshBuffer(vertices, indices);

	}

}