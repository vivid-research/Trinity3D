#pragma once
#include "MeshBuffer.h"

namespace Data {
	class Mesh3D
	{
	public:

		Mesh3D(int vertices, int indices);

		static Mesh3D* CreateCube(float size);
		Backend::MeshBuffer* GetBuffer() {
			return buffer;
		}

		void SetVertex(int id, Vertex3D vert);
		void SetTri(int id, Tri tri);

		void Finalize() {

			buffer->Finalize();

		}

	private:
		Backend::MeshBuffer* buffer;
	};


}