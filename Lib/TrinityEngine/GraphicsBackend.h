#pragma once
#include "MeshBuffer.h"

namespace Backend {

	class GraphicsBackend
	{
	public:

		virtual void CreateWindow(int w, int h, bool fullscreen) {};

		virtual void BeginFrame() {};
		virtual void EndFrame() {};
		virtual void SwapBuffers() {};
		virtual void SetClearColor(float r, float g, float a) {};
		virtual MeshBuffer* CreateMeshBuffer(int vertices, int indices) { return nullptr; };
		

	private:

		//Application* app;

	};

}

