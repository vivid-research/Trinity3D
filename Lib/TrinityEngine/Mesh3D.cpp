#include "pch.h"
#include "Mesh3D.h"
#include "Application.h"

namespace Data {

	Mesh3D::Mesh3D(int vertices, int tris) {

		printf("Creating mesh. Verts:%d Tris:%d\n", vertices, tris / 3);
		buffer = App::Application::Main->CreateMeshBuffer(vertices, tris);
		//vertices.


	}

	Mesh3D* Mesh3D::CreateCube(float size) {

		Mesh3D* res = new Mesh3D(8, 36);

		Vertex3D v1, v2, v3, v4, v5, v6, v7, v8;

		v1.Pos = glm::vec3(-1, -1, -1);
		v1.Color = glm::vec4(1, 1, 1,1);

		v2.Pos = glm::vec3(-1, 1, -1);
		v2.Color = v1.Color;

		v3.Pos = glm::vec3(1, 1, -1);
		v3.Color = v2.Color;

		v4.Pos = glm::vec3(1, -1, -1);
		v4.Color = v3.Color;

		v5.Pos = glm::vec3(-1, -1, 1);
		v5.Color = v4.Color;

		v6.Pos = glm::vec3(-1, 1, 1);
		v6.Color = v5.Color;

		v7.Pos = glm::vec3(1, 1, 1);
		v7.Color = v6.Color;

		v8.Pos = glm::vec3(1, -1, 1);
		v8.Color = v7.Color;

		res->SetVertex(0, v1);
		res->SetVertex(1, v2);
		res->SetVertex(2, v3);
		res->SetVertex(3, v4);
		res->SetVertex(4, v5);
		res->SetVertex(5, v6);
		res->SetVertex(6, v7);
		res->SetVertex(7, v8);

		Tri t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12;

		t1.V0 = 0;
		t1.V1 = 1;
		t1.V2 = 2;

		t2.V0 = 0;
		t2.V1 = 2;
		t2.V2 = 3;

		t3.V0 = 4;
		t3.V1 = 6;
		t3.V2 = 5;

		t4.V0 = 4;
		t4.V1 = 7;
		t4.V2 = 6;

		t5.V0 = 4;
		t5.V1 = 5;
		t5.V2 = 1;

		t6.V0 = 4;
		t6.V1 = 1;
		t6.V2 = 0;

		t7.V0 = 3;
		t7.V1 = 2;
		t7.V2 = 6;

		t8.V0 = 3;
		t8.V1 = 6;
		t8.V2 = 7;

		t9.V0 = 1;
		t9.V1= 5;
		t9.V2 = 6;

		t10.V0 = 1;
		t10.V1 = 6;
		t10.V2 = 2;

		t11.V0 = 4;
		t11.V1 = 0;
		t11.V2 = 3;

		t12.V0 = 4;
		t12.V1 = 3;
		t12.V2 = 7;
		


		res->SetTri(0, t1);
		res->SetTri(1, t2);
		res->SetTri(2, t3);
		res->SetTri(3, t4);
		res->SetTri(4, t5);
		res->SetTri(5, t6);
		res->SetTri(6, t7);
		res->SetTri(7, t8);
		res->SetTri(8, t9);
		res->SetTri(9, t10);
		res->SetTri(10, t11);
		res->SetTri(11, t12);

		res->Finalize();
	
		return res;

	}
		
	void Mesh3D::SetVertex(int id, Vertex3D v) {

		buffer->SetVertex(id, v);

	}

	void Mesh3D::SetTri(int id, Tri t) {

		buffer->SetTri(id, t);

	}

}