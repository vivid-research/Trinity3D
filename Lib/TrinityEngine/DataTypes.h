#pragma once
#include <glm/glm.hpp>

struct Vertex3D {

	glm::vec3 Pos;
	glm::vec4 Color;
	glm::vec3 TexCoord;
	glm::vec3 Normal;
	glm::vec3 BiNormal;
	glm::vec3 Tangent;

};

struct Tri {

	int V0;
	int V1;
	int V2;

};