#pragma once
#include "GraphicsBackend.h"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <shellapi.h> // For CommandLineToArgvW

// The min/max macros conflict with like-named member functions.
// Only use std::min and std::max defined in <algorithm>.
#if defined(min)
#undef min
#endif

#if defined(max)
#undef max
#endif

// In order to define a function called CreateWindow, the Windows macro needs to
// be undefined.
#if defined(CreateWindow)
#undef CreateWindow
#endif

// Windows Runtime Library. Needed for Microsoft::WRL::ComPtr<> template class.
#include <wrl.h>
using namespace Microsoft::WRL;

// DirectX 12 specific headers.
#include <d3d12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

// D3D12 extension library.
#include "d3dx12.h"
// STL Headers
#include <algorithm>
#include <cassert>
#include <chrono>
// Helper functions
#include "Helpers.h"

#include <glfw/glfw3.h>
#define GLFW_EXPOSE_NATIVE_WIN32
#include <glfw/glfw3native.h>
#include "CommandQueueDX12.h"

namespace Backend {

    using namespace DirectX;

    struct Vertex3D {

        XMFLOAT3 Position;
        XMFLOAT3 Color;

    };


    const uint8_t g_NumFrames = 3;

    // Window callback function.
    static constexpr DXGI_FORMAT g_BackBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
    static constexpr DXGI_FORMAT g_DepthStencilFormat =
        DXGI_FORMAT_D24_UNORM_S8_UINT;
    static constexpr UINT g_iSwapChainBufferCount = 2;

    void EnableDebugLayer();


    class GBEDirectX12 :
        public GraphicsBackend
    {
    public:

        static const UINT BufferCount = 3;

        
        GBEDirectX12();

        static GBEDirectX12* Main;

        void CreateWindow(int w, int h, bool fullscreen);

        void BeginFrame();
        void EndFrame();
        void SwapBuffers();

        void SetClearColor(float r, float g, float b) {

            clearR = r;
            clearG = g;
            clearB = b;

        }

        void FlushCommandQueue();

        std::shared_ptr<CommandQueueDX12> GetCommandQueue(D3D12_COMMAND_LIST_TYPE type = D3D12_COMMAND_LIST_TYPE_DIRECT) const;
    
        MeshBuffer* CreateMeshBuffer(int vertices, int indices);


        // Transition a resource
        void TransitionResource(Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList2> commandList,
            Microsoft::WRL::ComPtr<ID3D12Resource> resource,
            D3D12_RESOURCE_STATES beforeState, D3D12_RESOURCE_STATES afterState)
        {
            CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(
                resource.Get(),
                beforeState, afterState);

            commandList->ResourceBarrier(1, &barrier);
        }

        // Clear a render target.
        void ClearRTV(Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList2> commandList,
            D3D12_CPU_DESCRIPTOR_HANDLE rtv, FLOAT* clearColor)
        {
            commandList->ClearRenderTargetView(rtv, clearColor, 0, nullptr);
        }

        void ClearDepth(Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList2> commandList,
            D3D12_CPU_DESCRIPTOR_HANDLE dsv, FLOAT depth)
        {
            commandList->ClearDepthStencilView(dsv, D3D12_CLEAR_FLAG_DEPTH, depth, 0, 0, nullptr);
        }



        // Create a GPU buffer.

        void UpdateBufferResource(
            ComPtr<ID3D12GraphicsCommandList2> commandList,
            ID3D12Resource** pDestinationResource,
            ID3D12Resource** pIntermediateResource,
            size_t numElements, size_t elementSize, const void* bufferData,
            D3D12_RESOURCE_FLAGS flags)
        {
            auto device = m_d3d12Device;

            size_t bufferSize = numElements * elementSize;

            // Create a committed resource for the GPU resource in a default heap.
            ThrowIfFailed(device->CreateCommittedResource(
                &CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
                D3D12_HEAP_FLAG_NONE,
                &CD3DX12_RESOURCE_DESC::Buffer(bufferSize, flags),
                D3D12_RESOURCE_STATE_COPY_DEST,
                nullptr,
                IID_PPV_ARGS(pDestinationResource)));

            // Create an committed resource for the upload.
            if (bufferData)
            {
                ThrowIfFailed(device->CreateCommittedResource(
                    &CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
                    D3D12_HEAP_FLAG_NONE,
                    &CD3DX12_RESOURCE_DESC::Buffer(bufferSize),
                    D3D12_RESOURCE_STATE_GENERIC_READ,
                    nullptr,
                    IID_PPV_ARGS(pIntermediateResource)));

                D3D12_SUBRESOURCE_DATA subresourceData = {};
                subresourceData.pData = bufferData;
                subresourceData.RowPitch = bufferSize;
                subresourceData.SlicePitch = subresourceData.RowPitch;

                UpdateSubresources(commandList.Get(),
                    *pDestinationResource, *pIntermediateResource,
                    0, 0, 1, &subresourceData);
            }
        }

        // Resize the depth buffer to match the size of the client area.
        void ResizeDepthBuffer(int width, int height);
        Microsoft::WRL::ComPtr<ID3D12Device2> CreateDevice(Microsoft::WRL::ComPtr<IDXGIAdapter4> adapter);
        Microsoft::WRL::ComPtr<IDXGIAdapter4> GetAdapter(bool bUseWarp);
        void InitDirect3D(HWND hwnd, int width, int height);
        Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> CreateDescriptorHeap(UINT numDescriptors, D3D12_DESCRIPTOR_HEAP_TYPE type)
        {
            D3D12_DESCRIPTOR_HEAP_DESC desc = {};
            desc.Type = type;
            desc.NumDescriptors = numDescriptors;
            desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
            desc.NodeMask = 0;

            Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> descriptorHeap;
            ThrowIfFailed(m_d3d12Device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&descriptorHeap)));

            return descriptorHeap;
        }

        UINT GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE type) const
        {
            return m_d3d12Device->GetDescriptorHandleIncrementSize(type);
        }

        D3D12_CPU_DESCRIPTOR_HANDLE GetCurrentRenderTargetView() const;

        Microsoft::WRL::ComPtr<ID3D12Resource> GetCurrentBackBuffer() const;

            UINT GetCurrentBackBufferIndex() const;


            UINT GBEDirectX12::Present();


        Microsoft::WRL::ComPtr<IDXGISwapChain4> CreateSwapChain();
        void UpdateRenderTargetViews();

        void GBEDirectX12::OnResize(int w, int h);

        Microsoft::WRL::ComPtr<IDXGISwapChain4> m_dxgiSwapChain;
        Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_d3d12RTVDescriptorHeap;
        Microsoft::WRL::ComPtr<ID3D12Resource> m_d3d12BackBuffers[BufferCount];

    
        UINT m_CurrentBackBufferIndex;

        RECT m_WindowRect;
        // The number of swap chain back buffers.
        
        float clearR, clearG, clearB;

        // Use WARP adapter
        bool g_UseWarp = false;

        HWND hwin;

        uint32_t g_ClientWidth = 1280;
        uint32_t g_ClientHeight = 720;

        // Set to true once the DX12 objects have been initialized.
        bool g_IsInitialized = false;
        // Window handle.
        HWND g_hWnd;
        // Window rectangle (used to toggle fullscreen state).
        RECT g_WindowRect;

        // Command queue
        //ComPtr<ID3D12CommandQueue> g_CommandQueue;
        //ComPtr<ID3D12GraphicsCommandList> g_CommandList;
        //ComPtr<ID3D12CommandAllocator> g_DirectCmdAlloc;
        Microsoft::WRL::ComPtr<IDXGIAdapter4> m_dxgiAdapter;
        Microsoft::WRL::ComPtr<ID3D12Device2> m_d3d12Device;

        std::shared_ptr<CommandQueueDX12> m_DirectCommandQueue;
        std::shared_ptr<CommandQueueDX12> m_ComputeCommandQueue;
        std::shared_ptr<CommandQueueDX12> m_CopyCommandQueue;

        // Depth buffer.
        Microsoft::WRL::ComPtr<ID3D12Resource> m_DepthBuffer;
        // Descriptor heap for depth buffer.
        Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_DSVHeap;

        UINT m_RTVDescriptorSize;
        bool m_VSync;
        bool m_Fullscreen;
        bool m_IsTearingSupported;

        D3D12_VIEWPORT m_Viewport;
        D3D12_RECT m_ScissorRect;

        uint64_t m_FenceValues[BufferCount] = {};

//        ComPtr<ID3D12Device2> g_Device;
 //       ComPtr<IDXGISwapChain4> g_SwapChain;
  //      ComPtr<ID3D12Resource> g_BackBuffers[g_NumFrames];
   //     ComPtr<ID3D12DescriptorHeap> g_RTVDescriptorHeap;
   //     UINT g_RTVDescriptorSize;
    //    UINT g_CurrentBackBufferIndex;
    //    ComPtr<IDXGIFactory4> factory;
     //   ComPtr<IDXGIAdapter1> adapter;
  
        UINT g_uRtvDescriptorSize;
        UINT g_uDsvDescriptorSize;
        UINT g_uCbvSrvUavDescriptorSize;
       // uint64_t g_FenceValue = 0;
       // uint64_t g_FrameFenceValues[g_NumFrames] = {};
       // HANDLE g_FenceEvent;
        // By default, enable V-Sync.
// Can be toggled with the V key.
        bool g_VSync = true;
        bool g_TearingSupported = false;

        INT64 g_iFencePoint = 0;
        ComPtr<ID3D12Fence> g_pd3dFence;
        HANDLE g_hFenceEvent;

        // By default, use windowed mode.
        // Can be toggled with the Alt+Enter or F11
        bool g_Fullscreen = false;
        


         UINT g_iCurrentFrameIndex = 0;

         D3D12_VIEWPORT g_ScreenViewport;
         D3D12_RECT g_ScreenScissorRect;


    };


}
