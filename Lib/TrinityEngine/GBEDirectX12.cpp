#include "pch.h"
#include "GBEDirectX12.h"
#include "d3dx12.h"
#include <iostream>
#include <d3d12.h>
#include <comdef.h>      // for _com_error
#include "MeshBufferDX12.h"
namespace Backend {

	GBEDirectX12* GBE;



	void EnableDebugLayer()
	{
#if defined(_DEBUG)
		// Always enable the debug layer before doing anything DX12 related
		// so all possible errors generated while creating DX12 objects
		// are caught by the debug layer.
		ComPtr<ID3D12Debug> debugInterface;
		ThrowIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugInterface)));
		debugInterface->EnableDebugLayer();
#endif
	}

	void DxTrace(const wchar_t* file, unsigned long line, HRESULT hr,
		const wchar_t* proc) {
		_com_error err(hr);
		std::cerr << "file:" << file << "line:" << line << ", " << proc
			<< " Error: " << (const char*)err.Description() << std::endl;
	}

#define V_RETURN(op)                                                           \
  if (FAILED(hr = (op))) {                                                     \
    assert(0);                                                                 \
    DxTrace(__FILEW__, __LINE__, hr, L#op);                                    \
    return hr;                                                                 \
  }

#define V(op)                                                                  \
  if (FAILED(hr = (op))) {                                                     \
    assert(0);                                                                 \
    DxTrace(__FILEW__, __LINE__, hr, L#op);                                    \
  }


	Microsoft::WRL::ComPtr<ID3D12Device2> GBEDirectX12::CreateDevice(Microsoft::WRL::ComPtr<IDXGIAdapter4> adapter)
	{
		ComPtr<ID3D12Device2> d3d12Device2;
		ThrowIfFailed(D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&d3d12Device2)));
		//    NAME_D3D12_OBJECT(d3d12Device2);

			// Enable debug messages in debug mode.
#if defined(_DEBUG)
		ComPtr<ID3D12InfoQueue> pInfoQueue;
		if (SUCCEEDED(d3d12Device2.As(&pInfoQueue)))
		{
			pInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_CORRUPTION, TRUE);
			pInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_ERROR, TRUE);
			pInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_WARNING, TRUE);

			// Suppress whole categories of messages
			//D3D12_MESSAGE_CATEGORY Categories[] = {};

			// Suppress messages based on their severity level
			D3D12_MESSAGE_SEVERITY Severities[] =
			{
				D3D12_MESSAGE_SEVERITY_INFO
			};

			// Suppress individual messages by their ID
			D3D12_MESSAGE_ID DenyIds[] = {
				D3D12_MESSAGE_ID_CLEARRENDERTARGETVIEW_MISMATCHINGCLEARVALUE,   // I'm really not sure how to avoid this message.
				D3D12_MESSAGE_ID_MAP_INVALID_NULLRANGE,                         // This warning occurs when using capture frame while graphics debugging.
				D3D12_MESSAGE_ID_UNMAP_INVALID_NULLRANGE,                       // This warning occurs when using capture frame while graphics debugging.
			};

			D3D12_INFO_QUEUE_FILTER NewFilter = {};
			//NewFilter.DenyList.NumCategories = _countof(Categories);
			//NewFilter.DenyList.pCategoryList = Categories;
			NewFilter.DenyList.NumSeverities = _countof(Severities);
			NewFilter.DenyList.pSeverityList = Severities;
			NewFilter.DenyList.NumIDs = _countof(DenyIds);
			NewFilter.DenyList.pIDList = DenyIds;

			ThrowIfFailed(pInfoQueue->PushStorageFilter(&NewFilter));
		}
#endif

		return d3d12Device2;
	}

	Microsoft::WRL::ComPtr<IDXGIAdapter4> GBEDirectX12::GetAdapter(bool bUseWarp)
	{
		ComPtr<IDXGIFactory4> dxgiFactory;
		UINT createFactoryFlags = 0;
#if defined(_DEBUG)
		createFactoryFlags = DXGI_CREATE_FACTORY_DEBUG;
#endif

		ThrowIfFailed(CreateDXGIFactory2(createFactoryFlags, IID_PPV_ARGS(&dxgiFactory)));

		ComPtr<IDXGIAdapter1> dxgiAdapter1;
		ComPtr<IDXGIAdapter4> dxgiAdapter4;

		if (bUseWarp)
		{
			ThrowIfFailed(dxgiFactory->EnumWarpAdapter(IID_PPV_ARGS(&dxgiAdapter1)));
			ThrowIfFailed(dxgiAdapter1.As(&dxgiAdapter4));
		}
		else
		{
			SIZE_T maxDedicatedVideoMemory = 0;
			for (UINT i = 0; dxgiFactory->EnumAdapters1(i, &dxgiAdapter1) != DXGI_ERROR_NOT_FOUND; ++i)
			{
				DXGI_ADAPTER_DESC1 dxgiAdapterDesc1;
				dxgiAdapter1->GetDesc1(&dxgiAdapterDesc1);

				// Check to see if the adapter can create a D3D12 device without actually 
				// creating it. The adapter with the largest dedicated video memory
				// is favored.
				if ((dxgiAdapterDesc1.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) == 0 &&
					SUCCEEDED(D3D12CreateDevice(dxgiAdapter1.Get(),
						D3D_FEATURE_LEVEL_11_0, __uuidof(ID3D12Device), nullptr)) &&
					dxgiAdapterDesc1.DedicatedVideoMemory > maxDedicatedVideoMemory)
				{
					maxDedicatedVideoMemory = dxgiAdapterDesc1.DedicatedVideoMemory;
					ThrowIfFailed(dxgiAdapter1.As(&dxgiAdapter4));
				}
			}
		}

		return dxgiAdapter4;
	}

	///////////////


	Microsoft::WRL::ComPtr<IDXGISwapChain4> GBEDirectX12::CreateSwapChain()
	{
		//Application& app = Application::Get();


		ComPtr<IDXGISwapChain4> dxgiSwapChain4;
		ComPtr<IDXGIFactory4> dxgiFactory4;
		UINT createFactoryFlags = 0;
#if defined(_DEBUG)
		createFactoryFlags = DXGI_CREATE_FACTORY_DEBUG;
#endif

		ThrowIfFailed(CreateDXGIFactory2(createFactoryFlags, IID_PPV_ARGS(&dxgiFactory4)));

		DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
		swapChainDesc.Width = GBE->g_ClientWidth;//  m_ClientWidth;
		swapChainDesc.Height = GBE->g_ClientHeight;
		swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		swapChainDesc.Stereo = FALSE;
		swapChainDesc.SampleDesc = { 1, 0 };
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.BufferCount = BufferCount;
		swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
		// It is recommended to always allow tearing if tearing support is available.
		swapChainDesc.Flags = 0;
		ID3D12CommandQueue* pCommandQueue = GetCommandQueue()->GetD3D12CommandQueue().Get();

		ComPtr<IDXGISwapChain1> swapChain1;
		ThrowIfFailed(dxgiFactory4->CreateSwapChainForHwnd(
			pCommandQueue,
			hwin,
			&swapChainDesc,
			nullptr,
			nullptr,
			&swapChain1));

		// Disable the Alt+Enter fullscreen toggle feature. Switching to fullscreen
		// will be handled manually.
		ThrowIfFailed(dxgiFactory4->MakeWindowAssociation( hwin, DXGI_MWA_NO_ALT_ENTER));

		ThrowIfFailed(swapChain1.As(&dxgiSwapChain4));

		m_CurrentBackBufferIndex = dxgiSwapChain4->GetCurrentBackBufferIndex();

		return dxgiSwapChain4;
	}

	// Update the render target views for the swapchain back buffers.
	void GBEDirectX12::UpdateRenderTargetViews()
	{
		auto device = m_d3d12Device;

		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(m_d3d12RTVDescriptorHeap->GetCPUDescriptorHandleForHeapStart());

		for (int i = 0; i < BufferCount; ++i)
		{
			ComPtr<ID3D12Resource> backBuffer;
			ThrowIfFailed(m_dxgiSwapChain->GetBuffer(i, IID_PPV_ARGS(&backBuffer)));

			device->CreateRenderTargetView(backBuffer.Get(), nullptr, rtvHandle);

			m_d3d12BackBuffers[i] = backBuffer;

			rtvHandle.Offset(m_RTVDescriptorSize);
		}
	}

	D3D12_CPU_DESCRIPTOR_HANDLE GBEDirectX12::GetCurrentRenderTargetView() const
	{
		return CD3DX12_CPU_DESCRIPTOR_HANDLE(m_d3d12RTVDescriptorHeap->GetCPUDescriptorHandleForHeapStart(),
			m_CurrentBackBufferIndex, m_RTVDescriptorSize);
	}

	Microsoft::WRL::ComPtr<ID3D12Resource> GBEDirectX12::GetCurrentBackBuffer() const
	{
		return m_d3d12BackBuffers[m_CurrentBackBufferIndex];
	}

	UINT GBEDirectX12::GetCurrentBackBufferIndex() const
	{
		return m_CurrentBackBufferIndex;
	}

	UINT GBEDirectX12::Present()
	{
		UINT syncInterval = m_VSync ? 1 : 0;
		UINT presentFlags = m_IsTearingSupported && !m_VSync ? DXGI_PRESENT_ALLOW_TEARING : 0;
		ThrowIfFailed(m_dxgiSwapChain->Present(syncInterval, presentFlags));
		m_CurrentBackBufferIndex = m_dxgiSwapChain->GetCurrentBackBufferIndex();

		return m_CurrentBackBufferIndex;
	}








	/// <summary>
	/// /
	/// </summary>
	/// <param name="hwnd"></param>
	/// <param name="width"></param>
	/// <param name="height"></param>
	void GBEDirectX12::InitDirect3D(HWND hwnd, int width, int height) {

		m_dxgiAdapter = GetAdapter(false);
		if (m_dxgiAdapter)
		{
			m_d3d12Device = CreateDevice(m_dxgiAdapter);
		}
		if (m_d3d12Device)
		{
			m_DirectCommandQueue = std::make_shared<CommandQueueDX12>(m_d3d12Device, D3D12_COMMAND_LIST_TYPE_DIRECT);
			m_ComputeCommandQueue = std::make_shared<CommandQueueDX12>(m_d3d12Device, D3D12_COMMAND_LIST_TYPE_COMPUTE);
			m_CopyCommandQueue = std::make_shared<CommandQueueDX12>(m_d3d12Device, D3D12_COMMAND_LIST_TYPE_COPY);

			//m_TearingSupported = CheckTearingSupport();

		}
	}

	void GBEDirectX12::FlushCommandQueue() {

		m_DirectCommandQueue->Flush();
		m_ComputeCommandQueue->Flush();
		m_CopyCommandQueue->Flush();

	}



	std::shared_ptr<CommandQueueDX12> GBEDirectX12::GetCommandQueue(D3D12_COMMAND_LIST_TYPE type) const
	{
		std::shared_ptr<CommandQueueDX12> commandQueue;
		switch (type)
		{
		case D3D12_COMMAND_LIST_TYPE_DIRECT:
			commandQueue = m_DirectCommandQueue;
			break;
		case D3D12_COMMAND_LIST_TYPE_COMPUTE:
			commandQueue = m_ComputeCommandQueue;
			break;
		case D3D12_COMMAND_LIST_TYPE_COPY:
			commandQueue = m_CopyCommandQueue;
			break;
		default:
			assert(false && "Invalid command queue type.");
		}

		return commandQueue;
	}


	GBEDirectX12::GBEDirectX12() {

		GBE = this;
		Main = this;

	}

	void GBEDirectX12::OnResize(int w,int h)
	{

		m_Viewport = CD3DX12_VIEWPORT(0.0f, 0.0f,
			static_cast<float>(w), static_cast<float>(h));


		// Update the client size.
		if (g_ClientWidth != w || g_ClientHeight != h)
		{
			g_ClientWidth = std::max(1, w);
			g_ClientHeight = std::max(1, h);

			FlushCommandQueue();

			for (int i = 0; i < BufferCount; ++i)
			{
				m_d3d12BackBuffers[i].Reset();
			}

			DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
			ThrowIfFailed(m_dxgiSwapChain->GetDesc(&swapChainDesc));
			ThrowIfFailed(m_dxgiSwapChain->ResizeBuffers(BufferCount, g_ClientWidth,
				g_ClientHeight, swapChainDesc.BufferDesc.Format, swapChainDesc.Flags));

			m_CurrentBackBufferIndex = m_dxgiSwapChain->GetCurrentBackBufferIndex();

			UpdateRenderTargetViews();
			ResizeDepthBuffer(w,h);
		}

		
	}


	void OnResizeFrame(GLFWwindow* window, int width, int height) {
	
	}


	void GBEDirectX12::ResizeDepthBuffer(int width, int height)
	{

			// Flush any GPU commands that might be referencing the depth buffer.
		FlushCommandQueue();

			width = std::max(1, width);
			height = std::max(1, height);

			auto device = m_d3d12Device;

			// Resize screen dependent resources.
			// Create a depth buffer.
			D3D12_CLEAR_VALUE optimizedClearValue = {};
			optimizedClearValue.Format = DXGI_FORMAT_D32_FLOAT;
			optimizedClearValue.DepthStencil = { 1.0f, 0 };

			ThrowIfFailed(device->CreateCommittedResource(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Tex2D(DXGI_FORMAT_D32_FLOAT, width, height,
					1, 0, 1, 0, D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL),
				D3D12_RESOURCE_STATE_DEPTH_WRITE,
				&optimizedClearValue,
				IID_PPV_ARGS(&m_DepthBuffer)
			));

			// Update the depth-stencil view.
			D3D12_DEPTH_STENCIL_VIEW_DESC dsv = {};
			dsv.Format = DXGI_FORMAT_D32_FLOAT;
			dsv.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
			dsv.Texture2D.MipSlice = 0;
			dsv.Flags = D3D12_DSV_FLAG_NONE;

			device->CreateDepthStencilView(m_DepthBuffer.Get(), &dsv,
				m_DSVHeap->GetCPUDescriptorHandleForHeapStart());

	}



	void GBEDirectX12::CreateWindow(int w, int h, bool fs)
	{

		//g_ClientWidth = w;
		//g_ClientHeight = h;

		g_Fullscreen = fs;

		glfwInit();
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

		auto window =
			glfwCreateWindow(w, h, "DX12", nullptr, nullptr);
		if (!window) {
			std::cout << "Create window error" << std::endl;
			return;
		}

		auto hMainWnd = glfwGetWin32Window(window);
		hwin = hMainWnd;

		
		EnableDebugLayer();


		InitDirect3D(hMainWnd, w, h);

		m_dxgiSwapChain = CreateSwapChain();
		m_d3d12RTVDescriptorHeap = CreateDescriptorHeap(BufferCount, D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		m_RTVDescriptorSize = GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

		
	//	OnResizeFrame(window, w, h);
		   // Create the descriptor heap for the depth-stencil view.
		D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
		dsvHeapDesc.NumDescriptors = 1;
		dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		ThrowIfFailed(m_d3d12Device->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&m_DSVHeap)));
		OnResize(w, h);
		UpdateRenderTargetViews();

		printf("DirectX 12 Window created succesfully.\n");
	//	while (true) {

//		}

	};

	GBEDirectX12* GBEDirectX12::Main = nullptr;

	MeshBuffer* GBEDirectX12::CreateMeshBuffer(int vertices, int indices) {

		return (MeshBuffer*) new MeshBufferDX12(vertices, indices);

	}

	void GBEDirectX12::BeginFrame() {

	//	RenderFrame();
		auto commandQueue = GetCommandQueue(D3D12_COMMAND_LIST_TYPE_DIRECT);
		auto commandList = commandQueue->GetCommandList();

		UINT currentBackBufferIndex = GetCurrentBackBufferIndex();
		auto backBuffer = GetCurrentBackBuffer();
		auto rtv = GetCurrentRenderTargetView();
		auto dsv = m_DSVHeap->GetCPUDescriptorHandleForHeapStart();

		// Clear the render targets.
		{
			TransitionResource(commandList, backBuffer,
				D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);

			FLOAT clearColor[] = { 1, 0.6f, 0.9f, 1.0f };

			ClearRTV(commandList, rtv, clearColor);
			ClearDepth(commandList, dsv,1);
		}

		/*
		commandList->SetPipelineState(m_PipelineState.Get());
		commandList->SetGraphicsRootSignature(m_RootSignature.Get());

		commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		commandList->IASetVertexBuffers(0, 1, &m_VertexBufferView);
		commandList->IASetIndexBuffer(&m_IndexBufferView);

		commandList->RSSetViewports(1, &m_Viewport);
		commandList->RSSetScissorRects(1, &m_ScissorRect);

		commandList->OMSetRenderTargets(1, &rtv, FALSE, &dsv);

		// Update the MVP matrix
		XMMATRIX mvpMatrix = XMMatrixMultiply(m_ModelMatrix, m_ViewMatrix);
		mvpMatrix = XMMatrixMultiply(mvpMatrix, m_ProjectionMatrix);
		commandList->SetGraphicsRoot32BitConstants(0, sizeof(XMMATRIX) / 4, &mvpMatrix, 0);

		commandList->DrawIndexedInstanced(_countof(g_Indicies), 1, 0, 0, 0);
		*/

		// Present
		{
			TransitionResource(commandList, backBuffer,
				D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);

			m_FenceValues[currentBackBufferIndex] = commandQueue->ExecuteCommandList(commandList);

			currentBackBufferIndex = Present();

			commandQueue->WaitForFenceValue(m_FenceValues[currentBackBufferIndex]);
		}

	}
	void GBEDirectX12::EndFrame() {
		glfwPollEvents();

	}
	void GBEDirectX12::SwapBuffers() {

	}


}