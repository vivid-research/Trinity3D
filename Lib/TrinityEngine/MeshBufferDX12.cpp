#include "pch.h"
#include "MeshBufferDX12.h"

namespace Backend {

	void MeshBufferDX12::CreateBuffer(int vertices, int indices) {



	}

	void MeshBufferDX12::Finalize() {

		printf("Finalizing DX12 buffer.\n");

		auto commandQueue = GBEDirectX12::Main->GetCommandQueue(D3D12_COMMAND_LIST_TYPE_COPY);
		auto commandList = commandQueue->GetCommandList();

		auto device = GBEDirectX12::Main->m_d3d12Device;

		ComPtr<ID3D12Resource> intermediateVertexBuffer;
		UpdateBufferResource(commandList,
			&m_VertexBuffer, &intermediateVertexBuffer,
			(size_t)Verts.size(),(size_t)sizeof(dxVertex),Verts.data(),D3D12_RESOURCE_FLAGS::D3D12_RESOURCE_FLAG_NONE);

		// Create the vertex buffer view.
		m_VertexBufferView.BufferLocation = m_VertexBuffer->GetGPUVirtualAddress();
		m_VertexBufferView.SizeInBytes = Verts.size()*sizeof(dxVertex);
		m_VertexBufferView.StrideInBytes = sizeof(dxVertex);

		printf("Done!!\n");
	}

}