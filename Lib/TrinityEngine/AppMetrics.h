#pragma once
#include <string>

namespace App {

	enum GraphicsApi {

		OpenGL,DirectX12,DirectX11,Vulkan,Metal,OpenGLES

	};

	struct AppMetrics {

		int WindowWidth, WindowHeight;
		bool FullScreen;
		std::string Name;
		GraphicsApi Api;

		AppMetrics(int w, int h, bool fs, std::string name,GraphicsApi api) {

			WindowWidth = w;
			WindowHeight = h;
			FullScreen = fs;
			Name = name;
			Api = api;

		}

		AppMetrics() {

			WindowWidth = 800;
			WindowHeight = 600;
			FullScreen = false;
			Name = "Trinity Application";
			Api = GraphicsApi::DirectX12;

		}

	};


}