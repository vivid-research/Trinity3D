#include "pch.h"
#include "MeshBuffer.h"

namespace Backend {

	MeshBuffer::MeshBuffer(int vertices, int indices) {

		nVertices = vertices;
		nIndices = indices;
		CreateBuffer(vertices, indices);

	}
}