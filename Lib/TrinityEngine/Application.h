#pragma once
#include "AppMetrics.h"
#include "GraphicsBackend.h"
#include "MeshBuffer.h"

namespace App {

	class Application
	{
	public:

		Application();

		void SetMetrics(AppMetrics metrics);
		void RunApp();
		virtual void InitApp() {};
		virtual void UpdateApp() {};
		virtual void RenderApp() {};
		virtual void ResizeApp(int w, int h) {};
		Backend::MeshBuffer* CreateMeshBuffer(int vertices, int indices);

		static Application* Main;

	private:

		AppMetrics appMetrics;
		Backend::GraphicsBackend* gbe;

	};

}

