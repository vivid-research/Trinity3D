#include "pch.h"
#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <shellapi.h> // For CommandLineToArgvW

// The min/max macros conflict with like-named member functions.
// Only use std::min and std::max defined in <algorithm>.
#if defined(min)
#undef min
#endif

#if defined(max)
#undef max
#endif

// In order to define a function called CreateWindow, the Windows macro needs to
// be undefined.
#if defined(CreateWindow)
#undef CreateWindow
#endif

// Windows Runtime Library. Needed for Microsoft::WRL::ComPtr<> template class.
#include <wrl.h>
using namespace Microsoft::WRL;

// DirectX 12 specific headers.
#include <d3d12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

// D3D12 extension library.
#include "d3dx12.h"

// STL Headers
#include <algorithm>
#include <cassert>
#include <chrono>
#include <map>
#include <memory>

// Helper functions
#include "Helpers.h"

#include "MeshBuffer.h"
#include "GBEDirectX12.h"


namespace Backend {

    using namespace DirectX;

    struct dxVertex {

        XMFLOAT3 Position;
        XMFLOAT3 Color;

    };

    class MeshBufferDX12 :
        public MeshBuffer
    {
    public:
        MeshBufferDX12() {

        }
        MeshBufferDX12(int vertices, int indices)
        {
            Verts.resize(vertices);
            Indices.resize(indices);
            Vertices.resize(vertices);
            Tris.resize(indices/3);
            CreateBuffer(vertices, indices);
            printf("Creating MeshBufferDX12\n");
        }
        void CreateBuffer(int vertices, int indices);

        void Finalize();

        void UpdateBufferResource(
            ComPtr<ID3D12GraphicsCommandList2> commandList,
            ID3D12Resource** pDestinationResource,
            ID3D12Resource** pIntermediateResource,
            size_t numElements, size_t elementSize, const void* bufferData,
            D3D12_RESOURCE_FLAGS flags)

        {
            ComPtr<ID3D12Device2> device = Backend::GBEDirectX12::Main->m_d3d12Device;
                 
                //Application::Get().GetDevice();


            size_t bufferSize = numElements * elementSize;

            ThrowIfFailed(device->CreateCommittedResource(
                &CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
                D3D12_HEAP_FLAG_NONE,
                &CD3DX12_RESOURCE_DESC::Buffer(bufferSize, flags),
                D3D12_RESOURCE_STATE_COPY_DEST,
                nullptr,
                IID_PPV_ARGS(pDestinationResource)));
            // Create an committed resource for the upload.
            if (bufferData)
            {
                ThrowIfFailed(device->CreateCommittedResource(
                    &CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
                    D3D12_HEAP_FLAG_NONE,
                    &CD3DX12_RESOURCE_DESC::Buffer(bufferSize),
                    D3D12_RESOURCE_STATE_GENERIC_READ,
                    nullptr,
                    IID_PPV_ARGS(pIntermediateResource)));
                D3D12_SUBRESOURCE_DATA subresourceData = {};
                subresourceData.pData = bufferData;
                subresourceData.RowPitch = bufferSize;
                subresourceData.SlicePitch = subresourceData.RowPitch;

                UpdateSubresources(commandList.Get(),
                    *pDestinationResource, *pIntermediateResource,
                    0, 0, 1, &subresourceData);
            }
        }

    private:

        // Vertex buffer for the cube.
        Microsoft::WRL::ComPtr<ID3D12Resource> m_VertexBuffer;
        D3D12_VERTEX_BUFFER_VIEW m_VertexBufferView;
        // Index buffer for the cube.
        Microsoft::WRL::ComPtr<ID3D12Resource> m_IndexBuffer;
        D3D12_INDEX_BUFFER_VIEW m_IndexBufferView;
        // The VertexBuffer resource is used to store the vertex buffer g

        std::vector<dxVertex> Verts;
        std::vector<WORD> Indices;

    };

}
